package main

import (
	"context"
	"errors"
	"github.com/gin-gonic/gin"
	"lamoda-tech/internal/api"
	"lamoda-tech/internal/config"
	myDB "lamoda-tech/internal/pkg/db"
	"lamoda-tech/internal/repository"
	"lamoda-tech/internal/service"
	"log"
	"net/http"
	"os"
	"os/signal"
	"time"
)

func main() {
	cfg, err := config.NewConfig()
	if err != nil {
		log.Fatal(err)
	}

	db, err := myDB.OpenDB(context.Background(), cfg.DBConfig)
	if err != nil {
		log.Fatal(err)
	}
	defer func() {
		db.Close()
	}()

	gin.SetMode(cfg.ServerConfig.ServerMode) // set debug or release mode

	r := api.New(
		service.New(
			repository.New(db),
		),
	)

	srv := &http.Server{
		Addr:    ":" + cfg.ServerConfig.ServerPort,
		Handler: r,
	}
	go func() {
		if err := srv.ListenAndServe(); err != nil && !errors.Is(err, http.ErrServerClosed) {
			log.Fatalf("listen: %s\n", err)
		}
	}()

	waitForShutdown(srv)
}

func waitForShutdown(srv *http.Server) {
	quit := make(chan os.Signal)
	signal.Notify(quit, os.Interrupt)
	<-quit
	log.Println("Shutdown Server ...")

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	if err := srv.Shutdown(ctx); err != nil {
		log.Fatal("Server Shutdown:", err)
	}
	select {
	case <-ctx.Done():
		log.Println("Done")
	}
	log.Println("Server exiting")
}
