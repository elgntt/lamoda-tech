CREATE TABLE IF NOT EXISTS warehouses (
    id SERIAL PRIMARY KEY,
    name VARCHAR(255) NOT NULL,
    is_available BOOLEAN NOT NULL
);

CREATE TABLE IF NOT EXISTS products (
    id SERIAL PRIMARY KEY,
    name VARCHAR(255) NOT NULL,
    size VARCHAR(50) NOT NULL,
    unique_code VARCHAR(100) UNIQUE NOT NULL
);

CREATE TABLE IF NOT EXISTS reservations (
    id SERIAL PRIMARY KEY,
    product_id INT NOT NULL,
    warehouse_id INT NOT NULL REFERENCES warehouses(id),
    reserved_quantity INT NOT NULL CHECK (reserved_quantity >= 0),
    UNIQUE (product_id, warehouse_id)
);

CREATE TABLE warehouse_products (
    warehouse_id INT REFERENCES warehouses(id),
    product_id INT REFERENCES products(id),
    quantity INT NOT NULL CHECK (quantity >= 0)
);