.PHONY: up
up:
	docker-compose up -d --build

.PHONY: down
down:
	docker-compose down

.PHONY: test
test: ### run test
	go test -v ./...

.PHONY: cover-html
cover-html: ### run test with coverage and open html report
	go test -coverprofile=coverage.out ./...
	go tool cover -html=coverage.out
	rm coverage.out

.PHONY: cover
cover: ### run test with coverage
	go test -coverprofile=coverage.out ./...
	go tool cover -func=coverage.out
	rm coverage.out

.PHONY: mockgen
mockgen: ### generate mock
	mockgen -source=internal/service/deps.go 	-destination=internal/service/mocks_test.go -package=service
	mockgen -source=internal/api/deps.go 		-destination=internal/api/mocks_test.go -package=api
