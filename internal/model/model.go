package model

type ProductsUniqueCodes struct {
	ProductUniqueCodes []string `json:"productUniqueCodes"`
	WarehouseId        int      `json:"warehouseId"`
}

type Warehouse struct {
	Id          int
	Name        string
	IsAvailable bool
}

type Product struct {
	Id         int    `json:"id"`
	Name       string `json:"name"`
	Quantity   int    `json:"quantity"`
	UniqueCode string `json:"uniqueCode"`
}
