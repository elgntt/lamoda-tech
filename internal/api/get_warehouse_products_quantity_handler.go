package api

import (
	"github.com/gin-gonic/gin"
	response "lamoda-tech/internal/pkg/http"
	"net/http"
)

func (h *Handler) GetWarehouseProductsQuantity(c *gin.Context) {
	warehouseId, err := parseWarehouseIdParam(c.Param("warehouseId"))
	if err != nil {
		response.WriteErrorResponse(c, err)
		return
	}

	products, err := h.service.GetWarehouseProductsQuantity(c, warehouseId)
	if err != nil {
		response.WriteErrorResponse(c, err)
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"products": products,
	})
}
