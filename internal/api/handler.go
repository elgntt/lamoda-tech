package api

import (
	"github.com/gin-gonic/gin"
	"lamoda-tech/internal/model"
	"lamoda-tech/internal/pkg/app_err"
	"strconv"
)

type Handler struct {
	service Service
}

var (
	errInvalidWarehouseId      = "Invalid warehouseId"
	errInvalidWarehouseIdParam = "Invalid warehouseId in query"
	errEmptyUniqueCodesArray   = "An array of unique codes is not specified"
)

func New(service Service) *gin.Engine {
	h := Handler{
		service: service,
	}

	r := gin.New()

	r.POST("/products/reservations", h.ReserveProducts)
	r.DELETE("/products/reservations", h.ReleaseReservation)
	r.GET("/warehouses/:warehouseId/products", h.GetWarehouseProductsQuantity)

	return r
}

func parseWarehouseIdParam(warehouseIdQuery string) (int, error) {
	warehouseId, err := strconv.Atoi(warehouseIdQuery)
	if err != nil {
		return 0, app_err.NewBusinessError(errInvalidWarehouseIdParam)
	}
	if warehouseId < 1 {
		return 0, app_err.NewBusinessError(errInvalidWarehouseIdParam)
	}

	return warehouseId, nil
}

func parseReqData(req model.ProductsUniqueCodes) error {
	if req.WarehouseId < 1 {
		return app_err.NewBusinessError(errInvalidWarehouseId)
	}
	if len(req.ProductUniqueCodes) == 0 {
		return app_err.NewBusinessError(errEmptyUniqueCodesArray)
	}

	return nil
}
