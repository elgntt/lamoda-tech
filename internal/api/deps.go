package api

import (
	"context"
	"lamoda-tech/internal/model"
)

type Service interface {
	ReserveProducts(ctx context.Context, productsUniqueCodes []string, warehouseId int) error
	ReleaseReservation(ctx context.Context, productsUniqueCodes []string, warehouseId int) error
	GetWarehouseProductsQuantity(ctx context.Context, warehouseId int) ([]model.Product, error)
}
