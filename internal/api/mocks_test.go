// Code generated by MockGen. DO NOT EDIT.
// Source: internal/api/deps.go
//
// Generated by this command:
//
//	mockgen -source=internal/api/deps.go -destination=internal/api/mocks_test.go -package=api
//

// Package api is a generated GoMock package.
package api

import (
	context "context"
	model "lamoda-tech/internal/model"
	reflect "reflect"

	gomock "go.uber.org/mock/gomock"
)

// MockService is a mock of Service interface.
type MockService struct {
	ctrl     *gomock.Controller
	recorder *MockServiceMockRecorder
}

// MockServiceMockRecorder is the mock recorder for MockService.
type MockServiceMockRecorder struct {
	mock *MockService
}

// NewMockService creates a new mock instance.
func NewMockService(ctrl *gomock.Controller) *MockService {
	mock := &MockService{ctrl: ctrl}
	mock.recorder = &MockServiceMockRecorder{mock}
	return mock
}

// EXPECT returns an object that allows the caller to indicate expected use.
func (m *MockService) EXPECT() *MockServiceMockRecorder {
	return m.recorder
}

// GetWarehouseProductsQuantity mocks base method.
func (m *MockService) GetWarehouseProductsQuantity(ctx context.Context, warehouseId int) ([]model.Product, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "GetWarehouseProductsQuantity", ctx, warehouseId)
	ret0, _ := ret[0].([]model.Product)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// GetWarehouseProductsQuantity indicates an expected call of GetWarehouseProductsQuantity.
func (mr *MockServiceMockRecorder) GetWarehouseProductsQuantity(ctx, warehouseId any) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "GetWarehouseProductsQuantity", reflect.TypeOf((*MockService)(nil).GetWarehouseProductsQuantity), ctx, warehouseId)
}

// ReleaseReservation mocks base method.
func (m *MockService) ReleaseReservation(ctx context.Context, productsUniqueCodes []string, warehouseId int) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "ReleaseReservation", ctx, productsUniqueCodes, warehouseId)
	ret0, _ := ret[0].(error)
	return ret0
}

// ReleaseReservation indicates an expected call of ReleaseReservation.
func (mr *MockServiceMockRecorder) ReleaseReservation(ctx, productsUniqueCodes, warehouseId any) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "ReleaseReservation", reflect.TypeOf((*MockService)(nil).ReleaseReservation), ctx, productsUniqueCodes, warehouseId)
}

// ReserveProducts mocks base method.
func (m *MockService) ReserveProducts(ctx context.Context, productsUniqueCodes []string, warehouseId int) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "ReserveProducts", ctx, productsUniqueCodes, warehouseId)
	ret0, _ := ret[0].(error)
	return ret0
}

// ReserveProducts indicates an expected call of ReserveProducts.
func (mr *MockServiceMockRecorder) ReserveProducts(ctx, productsUniqueCodes, warehouseId any) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "ReserveProducts", reflect.TypeOf((*MockService)(nil).ReserveProducts), ctx, productsUniqueCodes, warehouseId)
}
