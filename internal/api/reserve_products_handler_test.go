package api

import (
	"bytes"
	"encoding/json"
	"errors"
	"github.com/gin-gonic/gin"
	"go.uber.org/mock/gomock"
	"lamoda-tech/internal/model"
	"log"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestHandler_ReserveProducts(t *testing.T) {
	existsWarehouseId := 1
	existsProductsUniqueCode := []string{"qwer1", "qwer2"}
	tests := []struct {
		name           string
		serviceBehave  func(service *MockService)
		wantStatusCode int
		wantErr        bool
		wantErrorText  string
		reqData        model.ProductsUniqueCodes
	}{
		{
			name: "success",
			serviceBehave: func(service *MockService) {
				service.EXPECT().ReserveProducts(gomock.Any(), existsProductsUniqueCode, existsWarehouseId)
			},
			reqData: model.ProductsUniqueCodes{
				ProductUniqueCodes: existsProductsUniqueCode,
				WarehouseId:        existsWarehouseId,
			},
			wantErr:        false,
			wantStatusCode: http.StatusOK,
		},
		{
			name: "invalid warehouse id",
			reqData: model.ProductsUniqueCodes{
				ProductUniqueCodes: existsProductsUniqueCode,
				WarehouseId:        0,
			},
			wantErrorText:  errInvalidWarehouseId,
			wantStatusCode: http.StatusBadRequest,
			wantErr:        true,
		},
		{
			name: "Empty products unique codes array",
			reqData: model.ProductsUniqueCodes{
				ProductUniqueCodes: []string{},
				WarehouseId:        existsWarehouseId,
			},
			wantErrorText:  errEmptyUniqueCodesArray,
			wantStatusCode: http.StatusBadRequest,
			wantErr:        true,
		}, {
			name: "server error",
			serviceBehave: func(service *MockService) {
				service.EXPECT().ReserveProducts(gomock.Any(), existsProductsUniqueCode, existsWarehouseId).Return(errors.New("some error"))
			},
			reqData: model.ProductsUniqueCodes{
				ProductUniqueCodes: existsProductsUniqueCode,
				WarehouseId:        existsWarehouseId,
			},
			wantErrorText:  "Something went wrong, please try again.",
			wantStatusCode: http.StatusInternalServerError,
			wantErr:        true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			ctrl := gomock.NewController(t)
			defer ctrl.Finish()

			mockService := NewMockService(ctrl)
			if tt.serviceBehave != nil {
				tt.serviceBehave(mockService)
			}
			gin.SetMode(gin.TestMode)

			router := gin.Default()
			recorder := httptest.NewRecorder()

			h := Handler{
				service: mockService,
			}
			reqJSON, err := json.Marshal(tt.reqData)
			if err != nil {
				log.Fatal(err)
			}
			router.POST("/products/reservations", h.ReserveProducts)
			req, _ := http.NewRequest("POST", "/products/reservations", bytes.NewBuffer(reqJSON))
			router.ServeHTTP(recorder, req)

			if recorder.Code != tt.wantStatusCode {
				t.Errorf("Handler returned wrong status code: got %v, want %v", recorder.Code, tt.wantStatusCode)
			}

			if tt.wantErr {
				var gotErrorRespBody errorResp
				err := json.Unmarshal(recorder.Body.Bytes(), &gotErrorRespBody)
				if err != nil {
					t.Errorf("Error decoding response body: %v", err)
				}

				// Сравниваем ожидаемое и фактическое тело ответа
				if gotErrorRespBody.Error.Message != tt.wantErrorText {
					t.Errorf("Handler returned wrong error text: got %v, want %v", gotErrorRespBody.Error.Message, tt.wantErrorText)
				}
			}
		})
	}
}
