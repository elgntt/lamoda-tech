package api

import (
	"github.com/gin-gonic/gin"
	"lamoda-tech/internal/model"
	response "lamoda-tech/internal/pkg/http"
	"net/http"
)

func (h *Handler) ReserveProducts(c *gin.Context) {
	req := model.ProductsUniqueCodes{}
	if err := c.BindJSON(&req); err != nil {
		response.WriteErrorResponse(c, err)
		return
	}

	if err := parseReqData(req); err != nil {
		response.WriteErrorResponse(c, err)
		return
	}

	err := h.service.ReserveProducts(c, req.ProductUniqueCodes, req.WarehouseId)
	if err != nil {
		response.WriteErrorResponse(c, err)
		return
	}

	c.Status(http.StatusOK)
}
