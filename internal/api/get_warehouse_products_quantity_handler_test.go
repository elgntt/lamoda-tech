package api

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/gin-gonic/gin"
	"go.uber.org/mock/gomock"
	"lamoda-tech/internal/model"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestHandler_GetWarehouseProductsQuantity(t *testing.T) {
	validWarehouseId := 12
	tests := []struct {
		name           string
		serviceBehave  func(service *MockService)
		warehouseId    int
		wantStatusCode int
		wantErrorText  string
		wantErr        bool
	}{
		{
			name: "success",
			serviceBehave: func(service *MockService) {
				service.EXPECT().GetWarehouseProductsQuantity(gomock.Any(), validWarehouseId).Return([]model.Product{
					{
						Id:       1,
						Name:     "Рубашка",
						Quantity: 10,
					},
				}, nil)
			},
			warehouseId:    validWarehouseId,
			wantStatusCode: http.StatusOK,
			wantErr:        false,
		},
		{
			name:           "invalid number in the warehouse id",
			serviceBehave:  func(service *MockService) {},
			warehouseId:    0,
			wantErrorText:  errInvalidWarehouseIdParam,
			wantStatusCode: http.StatusBadRequest,
			wantErr:        true,
		},
		{
			name:        "server error",
			warehouseId: validWarehouseId,
			serviceBehave: func(service *MockService) {
				service.EXPECT().GetWarehouseProductsQuantity(gomock.Any(), validWarehouseId).Return(nil, errors.New("some error"))
			},
			wantErrorText:  "Something went wrong, please try again.",
			wantStatusCode: http.StatusInternalServerError,
			wantErr:        true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			ctrl := gomock.NewController(t)
			defer ctrl.Finish()

			mockService := NewMockService(ctrl)
			if tt.serviceBehave != nil {
				tt.serviceBehave(mockService)
			}
			gin.SetMode(gin.TestMode)

			router := gin.Default()
			recorder := httptest.NewRecorder()

			h := Handler{
				service: mockService,
			}

			req, _ := http.NewRequest("GET", fmt.Sprintf("/warehouses/%d/products", tt.warehouseId), nil)
			router.GET("/warehouses/:warehouseId/products", h.GetWarehouseProductsQuantity)
			router.ServeHTTP(recorder, req)

			if recorder.Code != tt.wantStatusCode {
				t.Errorf("Handler returned wrong status code: got %v, want %v", recorder.Code, tt.wantStatusCode)
			}

			if tt.wantErr {
				var gotErrorRespBody errorResp
				err := json.Unmarshal(recorder.Body.Bytes(), &gotErrorRespBody)
				if err != nil {
					t.Errorf("Error decoding response body: %v", err)
				}

				// Сравниваем ожидаемое и фактическое тело ответа
				if gotErrorRespBody.Error.Message != tt.wantErrorText {
					t.Errorf("Handler returned wrong error text: got %v, want %v", gotErrorRespBody.Error.Message, tt.wantErrorText)
				}
			}
		})
	}
}
