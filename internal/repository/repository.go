package repository

import (
	"context"
	"errors"
	"fmt"
	"github.com/jackc/pgx/v5"
	"github.com/jackc/pgx/v5/pgxpool"
	"lamoda-tech/internal/model"
)

type Repo struct {
	pool *pgxpool.Pool
}

func New(pool *pgxpool.Pool) *Repo {
	return &Repo{
		pool: pool,
	}
}

func (r *Repo) GetWarehouse(ctx context.Context, warehouseId int) (*model.Warehouse, error) {
	query := `
		SELECT id, name, is_available
		FROM warehouses
		WHERE id = $1`

	var warehouse model.Warehouse
	row := r.pool.QueryRow(ctx, query, warehouseId)
	if err := row.Scan(
		&warehouse.Id,
		&warehouse.Name,
		&warehouse.IsAvailable,
	); err != nil {
		if errors.Is(err, pgx.ErrNoRows) {
			return nil, nil
		}
		return nil, fmt.Errorf("SQL: GetWarehouse: Scan: %w", err)
	}

	return &warehouse, nil
}

func (r *Repo) GetProductsQuantity(ctx context.Context, uniqueCodes []string, warehouseId int) (map[string]int, error) {
	query := `
		SELECT r.unique_code, wp.quantity
		FROM warehouse_products wp
		JOIN products r ON r.id = wp.product_id
		WHERE unique_code = ANY($1)
		AND warehouse_id = $2`

	rows, err := r.pool.Query(ctx, query, uniqueCodes, warehouseId)
	if err != nil {
		return nil, fmt.Errorf("SQL: GetProductsQuantity: Query: %w", err)
	}
	defer rows.Close()

	var productsQuantity = make(map[string]int)

	for rows.Next() {
		var (
			uniqCode string
			quantity int
		)
		err := rows.Scan(&uniqCode, &quantity)
		if err != nil {
			return nil, fmt.Errorf("SQL: GetProductsQuantity: Scan: %w", err)
		}
		productsQuantity[uniqCode] = quantity
	}
	if err := rows.Err(); err != nil {
		return nil, fmt.Errorf("SQL: GetProductsQuantity: rows.Err(): %w", err)
	}

	return productsQuantity, nil
}

func (r *Repo) GetReservedProducts(ctx context.Context, productUniqueCodes []string, warehouseId int) (map[string]int, error) {
	query := `
		SELECT p.unique_code, r.reserved_quantity
		FROM products p
		JOIN reservations r ON p.id = r.product_id
		WHERE p.unique_code = ANY($1)
		AND r.warehouse_id = $2
`

	rows, err := r.pool.Query(ctx, query, productUniqueCodes, warehouseId)
	if err != nil {
		return nil, fmt.Errorf("SQL: GetReservedProducts: Query: %w", err)
	}
	defer rows.Close()
	var existingProductsWithUniqueCode = make(map[string]int)

	for rows.Next() {
		var (
			uniqueCode       string
			reservedQuantity int
		)
		if err := rows.Scan(&uniqueCode, &reservedQuantity); err != nil {
			return nil, fmt.Errorf("SQL: GetReservedProducts: Scan: %w", err)
		}
		existingProductsWithUniqueCode[uniqueCode] = reservedQuantity
	}
	if err := rows.Err(); err != nil {
		return nil, fmt.Errorf("SQL: GetReservedProducts: rows.Err(): %w", err)
	}

	return existingProductsWithUniqueCode, nil
}

func (r *Repo) ReserveProducts(ctx context.Context, productUniqueCode []string, warehouseId int) (err error) {
	tx, err := r.pool.BeginTx(ctx, pgx.TxOptions{
		IsoLevel: pgx.RepeatableRead,
	})
	if err != nil {
		return fmt.Errorf("SQL: ReserveProducts: BeginTx: %w", err)
	}
	defer func() {
		if err != nil {
			_ = tx.Rollback(ctx)
		}
	}()

	_, err = tx.Exec(ctx, `
		UPDATE warehouse_products 
		SET quantity = quantity - 1
		WHERE product_id IN (
		    SELECT id
		    FROM products
		    WHERE unique_code = ANY($1)
		)
		AND warehouse_id = $2`, productUniqueCode, warehouseId)
	if err != nil {
		return fmt.Errorf("SQL: ReserveProducts: update products %w", err)
	}

	_, err = r.pool.Exec(ctx, `
		 INSERT INTO reservations (product_id, warehouse_id, reserved_quantity)
    		SELECT id, $1, 1
    		FROM products
    		WHERE unique_code = ANY($2)
    	 ON CONFLICT (product_id, warehouse_id) DO UPDATE
    	 SET reserved_quantity = reservations.reserved_quantity + 1`, warehouseId, productUniqueCode)
	if err != nil {
		return fmt.Errorf("SQL: ReserveProducts: insert into reservations %w", err)
	}

	err = tx.Commit(ctx)
	if err != nil {
		return fmt.Errorf("SQL: ReserveProducts: transaction commit %w", err)
	}

	return nil
}

func (r *Repo) ReleaseReservation(ctx context.Context, productUniqueCode []string, warehouseId int) (err error) {
	tx, err := r.pool.BeginTx(ctx, pgx.TxOptions{
		IsoLevel: pgx.RepeatableRead,
	})
	if err != nil {
		return fmt.Errorf("SQL: ReleaseProducts: BeginTx: %w", err)
	}
	defer func() {
		if err != nil {
			_ = tx.Rollback(ctx)
		}
	}()

	_, err = tx.Exec(ctx, `
		UPDATE warehouse_products 
		SET quantity = quantity + 1
		WHERE product_id IN (
		    SELECT id
		    FROM products
		    WHERE unique_code = ANY($1)
		)
		AND warehouse_id = $2`, productUniqueCode, warehouseId)
	if err != nil {
		return fmt.Errorf("SQL: ReleaseProducts: update warehouse_products: %w", err)
	}

	_, err = tx.Exec(ctx, `
		UPDATE reservations
		SET reserved_quantity = reserved_quantity - 1
		WHERE product_id IN (
		    SELECT id
		    FROM products
		    WHERE unique_code = ANY($1)
		)
		AND warehouse_id = $2`, productUniqueCode, warehouseId)
	if err != nil {
		return fmt.Errorf("SQL: ReleaseProducts: update reservations: %w", err)
	}

	err = tx.Commit(ctx)
	if err != nil {
		return fmt.Errorf("SQL: ReleaseProducts: transaction commit: %w", err)
	}

	return nil
}

func (r *Repo) GetWarehouseProductsQuantity(ctx context.Context, warehouseId int) ([]model.Product, error) {
	query := `
		SELECT p.id, p.name, wp.quantity, p.unique_code
		FROM warehouse_products wp
		JOIN products p ON wp.product_id = p.id
		WHERE wp.warehouse_id = $1`

	rows, err := r.pool.Query(ctx, query, warehouseId)
	if err != nil {
		return nil, fmt.Errorf("SQL: GetWarehouseProductsQuantity: Query: %w", err)
	}
	defer rows.Close()

	var products []model.Product
	for rows.Next() {
		var product model.Product
		if err := rows.Scan(
			&product.Id,
			&product.Name,
			&product.Quantity,
			&product.UniqueCode,
		); err != nil {
			return nil, fmt.Errorf("SQL: GetWarehouseProductsQuantity: Scan: %w", err)
		}
		products = append(products, product)
	}
	if err := rows.Err(); err != nil {
		return nil, fmt.Errorf("SQL: GetWarehouseProductsQuantity: rows.Err(): %w", err)
	}

	return products, nil
}
