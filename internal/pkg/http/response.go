package http

import (
	"errors"
	"lamoda-tech/internal/pkg/app_err"
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
)

type ErrorResponse struct {
	Error `json:"error"`
}

type Error struct {
	Code    string `json:"code"`
	Message string `json:"message"`
}

func WriteErrorResponse(c *gin.Context, err error) {
	var bErr app_err.BusinessError

	if errors.As(err, &bErr) {
		errorResponse := ErrorResponse{
			Error: Error{
				Code:    bErr.Code(),
				Message: bErr.Error(),
			},
		}

		log.Println(err)

		c.JSON(http.StatusBadRequest, errorResponse)
	} else {
		errorResponse := ErrorResponse{
			Error: Error{
				Code:    "InternalServerError",
				Message: "Something went wrong, please try again.",
			},
		}

		log.Println(err)

		c.JSON(http.StatusInternalServerError, errorResponse)
	}
}
