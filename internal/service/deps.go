package service

import (
	"context"
	"lamoda-tech/internal/model"
)

type Repository interface {
	GetProductsQuantity(ctx context.Context, uniqueCodes []string, warehouseId int) (map[string]int, error)
	ReserveProducts(ctx context.Context, productUniqueCode []string, warehouseId int) (err error)
	GetReservedProducts(ctx context.Context, productUniqueCodes []string, warehouseId int) (map[string]int, error)
	ReleaseReservation(ctx context.Context, productUniqueCode []string, warehouseId int) (err error)
	GetWarehouseProductsQuantity(ctx context.Context, warehouseId int) ([]model.Product, error)
	GetWarehouse(ctx context.Context, warehouseId int) (*model.Warehouse, error)
}
