package service

import (
	"context"
	"fmt"
	"lamoda-tech/internal/model"
	"lamoda-tech/internal/pkg/app_err"
)

type Service struct {
	repo Repository
}

func New(repo Repository) *Service {
	return &Service{
		repo: repo,
	}
}

func (s *Service) ReserveProducts(ctx context.Context, productsUniqueCodes []string, warehouseId int) error {
	err := s.checkWarehouseExists(ctx, warehouseId)
	if err != nil {
		return err
	}

	productsQuantity, err := s.repo.GetProductsQuantity(ctx, productsUniqueCodes, warehouseId)
	if err != nil {
		return err
	}

	for _, productCode := range productsUniqueCodes {
		if val, ok := productsQuantity[productCode]; !ok {
			return app_err.NewBusinessError(
				fmt.Sprintf(`The product with the code '%s' is not in this warehouse`, productCode),
			)
		} else if val < 1 {
			return app_err.NewBusinessError(
				fmt.Sprintf(`The product with the code '%s' is no longer in warehouse`, productCode),
			)
		}
	}
	return s.repo.ReserveProducts(ctx, productsUniqueCodes, warehouseId)
}

func (s *Service) ReleaseReservation(ctx context.Context, productsUniqueCodes []string, warehouseId int) error {
	err := s.checkWarehouseExists(ctx, warehouseId)
	if err != nil {
		return err
	}

	reservedProducts, err := s.repo.GetReservedProducts(ctx, productsUniqueCodes, warehouseId)
	if err != nil {
		return err
	}

	for _, productCode := range productsUniqueCodes {
		if val, ok := reservedProducts[productCode]; !ok || val < 1 {
			return app_err.NewBusinessError(
				fmt.Sprintf(`The product with the code '%s' is not in reserve`, productCode))
		}
	}
	return s.repo.ReleaseReservation(ctx, productsUniqueCodes, warehouseId)
}

func (s *Service) GetWarehouseProductsQuantity(ctx context.Context, warehouseId int) ([]model.Product, error) {
	err := s.checkWarehouseExists(ctx, warehouseId)
	if err != nil {
		return nil, err
	}

	productsQuantity, err := s.repo.GetWarehouseProductsQuantity(ctx, warehouseId)
	if err != nil {
		return nil, err
	}

	return productsQuantity, nil
}

func (s *Service) checkWarehouseExists(ctx context.Context, warehouseId int) error {
	warehouse, err := s.repo.GetWarehouse(ctx, warehouseId)
	if err != nil {
		return err
	}
	if warehouse == nil {
		return app_err.NewBusinessError("Warehouse not found")
	}
	if !warehouse.IsAvailable {
		return app_err.NewBusinessError("Warehouse not available")
	}
	return nil
}
