// Code generated by MockGen. DO NOT EDIT.
// Source: internal/service/deps.go
//
// Generated by this command:
//
//	mockgen -source=internal/service/deps.go -destination=internal/service/mocks_test.go -package=service
//

// Package service is a generated GoMock package.
package service

import (
	context "context"
	model "lamoda-tech/internal/model"
	reflect "reflect"

	gomock "go.uber.org/mock/gomock"
)

// MockRepository is a mock of Repository interface.
type MockRepository struct {
	ctrl     *gomock.Controller
	recorder *MockRepositoryMockRecorder
}

// MockRepositoryMockRecorder is the mock recorder for MockRepository.
type MockRepositoryMockRecorder struct {
	mock *MockRepository
}

// NewMockRepository creates a new mock instance.
func NewMockRepository(ctrl *gomock.Controller) *MockRepository {
	mock := &MockRepository{ctrl: ctrl}
	mock.recorder = &MockRepositoryMockRecorder{mock}
	return mock
}

// EXPECT returns an object that allows the caller to indicate expected use.
func (m *MockRepository) EXPECT() *MockRepositoryMockRecorder {
	return m.recorder
}

// GetProductsQuantity mocks base method.
func (m *MockRepository) GetProductsQuantity(ctx context.Context, uniqueCodes []string, warehouseId int) (map[string]int, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "GetProductsQuantity", ctx, uniqueCodes, warehouseId)
	ret0, _ := ret[0].(map[string]int)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// GetProductsQuantity indicates an expected call of GetProductsQuantity.
func (mr *MockRepositoryMockRecorder) GetProductsQuantity(ctx, uniqueCodes, warehouseId any) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "GetProductsQuantity", reflect.TypeOf((*MockRepository)(nil).GetProductsQuantity), ctx, uniqueCodes, warehouseId)
}

// GetReservedProducts mocks base method.
func (m *MockRepository) GetReservedProducts(ctx context.Context, productUniqueCodes []string, warehouseId int) (map[string]int, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "GetReservedProducts", ctx, productUniqueCodes, warehouseId)
	ret0, _ := ret[0].(map[string]int)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// GetReservedProducts indicates an expected call of GetReservedProducts.
func (mr *MockRepositoryMockRecorder) GetReservedProducts(ctx, productUniqueCodes, warehouseId any) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "GetReservedProducts", reflect.TypeOf((*MockRepository)(nil).GetReservedProducts), ctx, productUniqueCodes, warehouseId)
}

// GetWarehouse mocks base method.
func (m *MockRepository) GetWarehouse(ctx context.Context, warehouseId int) (*model.Warehouse, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "GetWarehouse", ctx, warehouseId)
	ret0, _ := ret[0].(*model.Warehouse)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// GetWarehouse indicates an expected call of GetWarehouse.
func (mr *MockRepositoryMockRecorder) GetWarehouse(ctx, warehouseId any) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "GetWarehouse", reflect.TypeOf((*MockRepository)(nil).GetWarehouse), ctx, warehouseId)
}

// GetWarehouseProductsQuantity mocks base method.
func (m *MockRepository) GetWarehouseProductsQuantity(ctx context.Context, warehouseId int) ([]model.Product, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "GetWarehouseProductsQuantity", ctx, warehouseId)
	ret0, _ := ret[0].([]model.Product)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// GetWarehouseProductsQuantity indicates an expected call of GetWarehouseProductsQuantity.
func (mr *MockRepositoryMockRecorder) GetWarehouseProductsQuantity(ctx, warehouseId any) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "GetWarehouseProductsQuantity", reflect.TypeOf((*MockRepository)(nil).GetWarehouseProductsQuantity), ctx, warehouseId)
}

// ReleaseReservation mocks base method.
func (m *MockRepository) ReleaseReservation(ctx context.Context, productUniqueCode []string, warehouseId int) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "ReleaseReservation", ctx, productUniqueCode, warehouseId)
	ret0, _ := ret[0].(error)
	return ret0
}

// ReleaseReservation indicates an expected call of ReleaseReservation.
func (mr *MockRepositoryMockRecorder) ReleaseReservation(ctx, productUniqueCode, warehouseId any) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "ReleaseReservation", reflect.TypeOf((*MockRepository)(nil).ReleaseReservation), ctx, productUniqueCode, warehouseId)
}

// ReserveProducts mocks base method.
func (m *MockRepository) ReserveProducts(ctx context.Context, productUniqueCode []string, warehouseId int) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "ReserveProducts", ctx, productUniqueCode, warehouseId)
	ret0, _ := ret[0].(error)
	return ret0
}

// ReserveProducts indicates an expected call of ReserveProducts.
func (mr *MockRepositoryMockRecorder) ReserveProducts(ctx, productUniqueCode, warehouseId any) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "ReserveProducts", reflect.TypeOf((*MockRepository)(nil).ReserveProducts), ctx, productUniqueCode, warehouseId)
}
