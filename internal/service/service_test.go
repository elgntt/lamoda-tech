package service

import (
	"context"
	"errors"
	"go.uber.org/mock/gomock"
	"lamoda-tech/internal/model"
	"reflect"
	"testing"
)

var (
	existsUniqueCodes       = []string{"qwer1", "qwer2", "qwer3"}
	notExistsUniqueCode     = []string{"qwer1", "qwer2", "qwer4"}
	allNotExistsUniqueCodes = []string{"1232qwe", "1233qwe", "1234qwe"}

	existsWarehouseId, notExistsWarehouseId, notAvailableWarehouseId = 1, 10231, 6
	warehouse                                                        = &model.Warehouse{
		Id:          1,
		Name:        "Example",
		IsAvailable: true,
	}
)

func TestService_ReserveProducts(t *testing.T) {
	type args struct {
		ctx                 context.Context
		productsUniqueCodes []string
		warehouseId         int
	}
	tests := []struct {
		name       string
		repoBehave func(repository *MockRepository)
		args       args

		wantErr bool
	}{
		{
			name: "success",
			args: args{
				ctx:                 context.Background(),
				productsUniqueCodes: existsUniqueCodes,
				warehouseId:         existsWarehouseId,
			},
			repoBehave: func(repository *MockRepository) {
				repository.EXPECT().GetWarehouse(gomock.Any(), existsWarehouseId).Return(warehouse, nil)
				repository.EXPECT().GetProductsQuantity(gomock.Any(), existsUniqueCodes, existsWarehouseId).Return(map[string]int{
					"qwer1": 2,
					"qwer2": 5,
					"qwer3": 7,
				}, nil)
				repository.EXPECT().ReserveProducts(gomock.Any(), existsUniqueCodes, existsWarehouseId).Return(nil)
			},
			wantErr: false,
		},
		{
			name: "Warehouse not exists",
			args: args{
				ctx:                 context.Background(),
				productsUniqueCodes: existsUniqueCodes,
				warehouseId:         notExistsWarehouseId,
			},
			repoBehave: func(repository *MockRepository) {
				repository.EXPECT().GetWarehouse(gomock.Any(), notExistsWarehouseId).Return(nil, nil)
			},
			wantErr: true,
		},
		{
			name: "Warehouse not available",
			args: args{
				ctx:                 context.Background(),
				productsUniqueCodes: existsUniqueCodes,
				warehouseId:         notAvailableWarehouseId,
			},
			repoBehave: func(repository *MockRepository) {
				repository.EXPECT().GetWarehouse(gomock.Any(), notAvailableWarehouseId).Return(&model.Warehouse{
					Id:          notAvailableWarehouseId,
					Name:        "Example",
					IsAvailable: false,
				}, nil)
			},
			wantErr: true,
		},
		{
			name: "Error from GetAvailableWarehouse",
			args: args{
				ctx:                 context.Background(),
				productsUniqueCodes: existsUniqueCodes,
				warehouseId:         existsWarehouseId,
			},
			repoBehave: func(repository *MockRepository) {
				repository.EXPECT().GetWarehouse(gomock.Any(), existsWarehouseId).Return(nil, errors.New("some error"))
			},
			wantErr: true,
		},
		{
			name: "Error from GetProductsQuantity",
			args: args{
				ctx:                 context.Background(),
				productsUniqueCodes: existsUniqueCodes,
				warehouseId:         existsWarehouseId,
			},
			repoBehave: func(repository *MockRepository) {
				repository.EXPECT().GetWarehouse(gomock.Any(), existsWarehouseId).Return(&model.Warehouse{
					Id:          1,
					Name:        "Example",
					IsAvailable: true,
				}, nil)
				repository.EXPECT().GetProductsQuantity(gomock.Any(), existsUniqueCodes, existsWarehouseId).Return(nil, errors.New("some error"))
			},
			wantErr: true,
		},
		{
			name: "Error from ReserveProducts",
			args: args{
				ctx:                 context.Background(),
				productsUniqueCodes: existsUniqueCodes,
				warehouseId:         existsWarehouseId,
			},
			repoBehave: func(repository *MockRepository) {
				repository.EXPECT().GetWarehouse(gomock.Any(), existsWarehouseId).Return(&model.Warehouse{
					Id:          1,
					Name:        "Example",
					IsAvailable: true,
				}, nil)
				repository.EXPECT().GetProductsQuantity(gomock.Any(), existsUniqueCodes, existsWarehouseId).Return(map[string]int{
					"qwer1": 2,
					"qwer2": 5,
					"qwer3": 7,
				}, nil)
				repository.EXPECT().ReserveProducts(gomock.Any(), existsUniqueCodes, existsWarehouseId).Return(errors.New("some error"))
			},
			wantErr: true,
		},
		{
			name: "Product with unique code not found",
			args: args{
				ctx:                 context.Background(),
				productsUniqueCodes: notExistsUniqueCode,
				warehouseId:         existsWarehouseId,
			},
			repoBehave: func(repository *MockRepository) {
				repository.EXPECT().GetWarehouse(gomock.Any(), existsWarehouseId).Return(&model.Warehouse{
					Id:          1,
					Name:        "Example",
					IsAvailable: true,
				}, nil)
				repository.EXPECT().GetProductsQuantity(gomock.Any(), notExistsUniqueCode, existsWarehouseId).Return(map[string]int{
					"qwer1": 1,
					"qwer2": 2,
				}, nil)
			},
			wantErr: true,
		},
		{
			name: "No products found in warehouse",
			args: args{
				ctx:                 context.Background(),
				productsUniqueCodes: allNotExistsUniqueCodes,
				warehouseId:         existsWarehouseId,
			},
			repoBehave: func(repository *MockRepository) {
				repository.EXPECT().GetWarehouse(gomock.Any(), existsWarehouseId).Return(&model.Warehouse{
					Id:          1,
					Name:        "Example",
					IsAvailable: true,
				}, nil)
				repository.EXPECT().GetProductsQuantity(gomock.Any(), allNotExistsUniqueCodes, existsWarehouseId).Return(map[string]int{}, nil)
			},
			wantErr: true,
		},
		{
			name: "The product is out of stock",
			args: args{
				ctx:                 context.Background(),
				productsUniqueCodes: existsUniqueCodes,
				warehouseId:         existsWarehouseId,
			},
			repoBehave: func(repository *MockRepository) {
				repository.EXPECT().GetWarehouse(gomock.Any(), existsWarehouseId).Return(&model.Warehouse{
					Id:          1,
					Name:        "Example",
					IsAvailable: true,
				}, nil)
				repository.EXPECT().GetProductsQuantity(gomock.Any(), existsUniqueCodes, existsWarehouseId).Return(map[string]int{
					"qwer1": 2,
					"qwer2": 0,
					"qwer3": 7,
				}, nil)
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			ctrl := gomock.NewController(t)
			defer ctrl.Finish()

			repoMock := NewMockRepository(ctrl)
			if tt.repoBehave != nil {
				tt.repoBehave(repoMock)
			}
			s := &Service{
				repo: repoMock,
			}
			if err := s.ReserveProducts(context.Background(), tt.args.productsUniqueCodes, tt.args.warehouseId); (err != nil) != tt.wantErr {
				t.Errorf("ReserveProducts() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestService_ReleaseReservation(t *testing.T) {
	type args struct {
		ctx                 context.Context
		productsUniqueCodes []string
		warehouseId         int
	}
	tests := []struct {
		name       string
		args       args
		repoBehave func(repository *MockRepository)
		wantErr    bool
	}{
		{
			name: "Success",
			args: args{
				ctx:                 context.Background(),
				productsUniqueCodes: existsUniqueCodes,
				warehouseId:         existsWarehouseId,
			},
			repoBehave: func(repository *MockRepository) {
				repository.EXPECT().GetWarehouse(gomock.Any(), existsWarehouseId).Return(warehouse, nil)
				repository.EXPECT().GetReservedProducts(gomock.Any(), existsUniqueCodes, existsWarehouseId).Return(map[string]int{
					"qwer1": 23,
					"qwer2": 2,
					"qwer3": 23,
				}, nil)
				repository.EXPECT().ReleaseReservation(gomock.Any(), existsUniqueCodes, existsWarehouseId).Return(nil)
			},
			wantErr: false,
		},
		{
			name: "Warehouse not exists",
			args: args{
				ctx:                 context.Background(),
				productsUniqueCodes: existsUniqueCodes,
				warehouseId:         notExistsWarehouseId,
			},
			repoBehave: func(repository *MockRepository) {
				repository.EXPECT().GetWarehouse(gomock.Any(), notExistsWarehouseId).Return(nil, nil)
			},
			wantErr: true,
		},
		{
			name: "Warehouse not available",
			args: args{
				ctx:                 context.Background(),
				productsUniqueCodes: existsUniqueCodes,
				warehouseId:         notAvailableWarehouseId,
			},
			repoBehave: func(repository *MockRepository) {
				repository.EXPECT().GetWarehouse(gomock.Any(), notAvailableWarehouseId).Return(&model.Warehouse{
					Id:          notAvailableWarehouseId,
					Name:        "Example",
					IsAvailable: false,
				}, nil)
			},
			wantErr: true,
		},
		{
			name: "Error from GetAvailableWarehouse",
			args: args{
				ctx:                 context.Background(),
				productsUniqueCodes: existsUniqueCodes,
				warehouseId:         existsWarehouseId,
			},
			repoBehave: func(repository *MockRepository) {
				repository.EXPECT().GetWarehouse(gomock.Any(), existsWarehouseId).Return(nil, errors.New("some error"))
			},
			wantErr: true,
		},
		{
			name: "Error from GetReservedProducts",
			args: args{
				ctx:                 context.Background(),
				productsUniqueCodes: existsUniqueCodes,
				warehouseId:         existsWarehouseId,
			},
			repoBehave: func(repository *MockRepository) {
				repository.EXPECT().GetWarehouse(gomock.Any(), existsWarehouseId).Return(warehouse, nil)
				repository.EXPECT().GetReservedProducts(gomock.Any(), existsUniqueCodes, existsWarehouseId).Return(nil, errors.New("some error"))
			},
			wantErr: true,
		},
		{
			name: "Error from ReleaseReservation",
			args: args{
				ctx:                 context.Background(),
				productsUniqueCodes: existsUniqueCodes,
				warehouseId:         existsWarehouseId,
			},
			repoBehave: func(repository *MockRepository) {
				repository.EXPECT().GetWarehouse(gomock.Any(), existsWarehouseId).Return(warehouse, nil)
				repository.EXPECT().GetReservedProducts(gomock.Any(), existsUniqueCodes, existsWarehouseId).Return(map[string]int{
					"qwer1": 23,
					"qwer2": 2,
					"qwer3": 23,
				}, nil)
				repository.EXPECT().ReleaseReservation(gomock.Any(), existsUniqueCodes, existsWarehouseId).Return(errors.New("some error"))
			},
			wantErr: true,
		},
		{
			name: "The product in reserve is over",
			args: args{
				ctx:                 context.Background(),
				productsUniqueCodes: []string{"qwer1", "qwer5"},
				warehouseId:         existsWarehouseId,
			},
			repoBehave: func(repository *MockRepository) {
				repository.EXPECT().GetWarehouse(gomock.Any(), existsWarehouseId).Return(warehouse, nil)
				repository.EXPECT().GetReservedProducts(gomock.Any(), []string{"qwer1", "qwer5"}, existsWarehouseId).Return(map[string]int{
					"qwer1": 23,
					"qwer5": 0,
				}, nil)
			},
			wantErr: true,
		},
		{
			name: "The product in reserve is not found",
			args: args{
				ctx:                 context.Background(),
				productsUniqueCodes: []string{"qwer1", "qwer6"},
				warehouseId:         existsWarehouseId,
			},
			repoBehave: func(repository *MockRepository) {
				repository.EXPECT().GetWarehouse(gomock.Any(), existsWarehouseId).Return(warehouse, nil)
				repository.EXPECT().GetReservedProducts(gomock.Any(), []string{"qwer1", "qwer6"}, existsWarehouseId).Return(map[string]int{
					"qwer1": 23,
				}, nil)
			},
			wantErr: true,
		},
		{
			name: "Products with unique codes not found in stock",
			args: args{
				ctx:                 context.Background(),
				productsUniqueCodes: notExistsUniqueCode,
				warehouseId:         existsWarehouseId,
			},
			repoBehave: func(repository *MockRepository) {
				repository.EXPECT().GetWarehouse(gomock.Any(), existsWarehouseId).Return(warehouse, nil)
				repository.EXPECT().GetReservedProducts(gomock.Any(), notExistsUniqueCode, existsWarehouseId).Return(map[string]int{}, nil)
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			ctrl := gomock.NewController(t)
			defer ctrl.Finish()

			repoMock := NewMockRepository(ctrl)
			if tt.repoBehave != nil {
				tt.repoBehave(repoMock)
			}
			s := &Service{
				repo: repoMock,
			}

			if err := s.ReleaseReservation(tt.args.ctx, tt.args.productsUniqueCodes, tt.args.warehouseId); (err != nil) != tt.wantErr {
				t.Errorf("ReleaseReservation() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestService_GetWarehouseProductsQuantity(t *testing.T) {
	type args struct {
		ctx         context.Context
		warehouseId int
	}
	tests := []struct {
		name       string
		args       args
		repoBehave func(repository *MockRepository)
		want       []model.Product
		wantErr    bool
	}{
		{
			name: "success",
			args: args{
				ctx:         context.Background(),
				warehouseId: existsWarehouseId,
			},
			repoBehave: func(repository *MockRepository) {
				repository.EXPECT().GetWarehouse(gomock.Any(), existsWarehouseId).Return(warehouse, nil)
				repository.EXPECT().GetWarehouseProductsQuantity(gomock.Any(), existsWarehouseId).Return([]model.Product{
					{
						Id:       1,
						Name:     "Рубашка",
						Quantity: 10,
					},
					{
						Id:       2,
						Name:     "Блузка",
						Quantity: 20,
					},
				}, nil)
			},
			wantErr: false,
			want: []model.Product{
				{
					Id:       1,
					Name:     "Рубашка",
					Quantity: 10,
				},
				{
					Id:       2,
					Name:     "Блузка",
					Quantity: 20,
				},
			},
		},
		{
			name: "There is no product in stock",
			args: args{
				ctx:         context.Background(),
				warehouseId: existsWarehouseId,
			},
			repoBehave: func(repository *MockRepository) {
				repository.EXPECT().GetWarehouse(gomock.Any(), existsWarehouseId).Return(warehouse, nil)
				repository.EXPECT().GetWarehouseProductsQuantity(gomock.Any(), existsWarehouseId).Return([]model.Product{}, nil)
			},
			wantErr: false,
			want:    []model.Product{},
		},
		{
			name: "Warehouse not exists",
			args: args{
				ctx:         context.Background(),
				warehouseId: notExistsWarehouseId,
			},
			repoBehave: func(repository *MockRepository) {
				repository.EXPECT().GetWarehouse(gomock.Any(), notExistsWarehouseId).Return(nil, nil)
			},
			wantErr: true,
			want:    nil,
		},
		{
			name: "Error from GetAvailableWarehouse",
			args: args{
				ctx:         context.Background(),
				warehouseId: existsWarehouseId,
			},
			repoBehave: func(repository *MockRepository) {
				repository.EXPECT().GetWarehouse(gomock.Any(), existsWarehouseId).Return(nil, errors.New("some error"))
			},
			wantErr: true,
			want:    nil,
		},
		{
			name: "Error from GetWarehouseProductsQuantity",
			args: args{
				ctx:         context.Background(),
				warehouseId: existsWarehouseId,
			},
			repoBehave: func(repository *MockRepository) {
				repository.EXPECT().GetWarehouse(gomock.Any(), existsWarehouseId).Return(warehouse, nil)
				repository.EXPECT().GetWarehouseProductsQuantity(gomock.Any(), existsWarehouseId).Return(nil, errors.New("some error"))
			},
			wantErr: true,
			want:    nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			ctrl := gomock.NewController(t)
			defer ctrl.Finish()

			repoMock := NewMockRepository(ctrl)
			if tt.repoBehave != nil {
				tt.repoBehave(repoMock)
			}
			s := &Service{
				repo: repoMock,
			}
			got, err := s.GetWarehouseProductsQuantity(tt.args.ctx, tt.args.warehouseId)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetWarehouseProductsQuantity() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetWarehouseProductsQuantity() got = %v, want %v", got, tt.want)
			}
		})
	}
}
